LOCAL_PATH:= $(call my-dir)

ifneq ($(filter beryllium,$(TARGET_DEVICE)),)

$(info "Including fw repo")

# firmware
$(call add-radio-file,abl_a.elf)
$(call add-radio-file,abl_b.elf)
$(call add-radio-file,aop.img)
$(call add-radio-file,cmnlib.img)
$(call add-radio-file,cmnlib64.img)
$(call add-radio-file,devcfg.img)
$(call add-radio-file,qupfw.img)
$(call add-radio-file,storsec.img)
$(call add-radio-file,tz.img)
$(call add-radio-file,xbl.img)
$(call add-radio-file,xbl_config.img)
$(call add-radio-file,keymaster.img)
$(call add-radio-file,bluetooth.img)
$(call add-radio-file,modem.img)
$(call add-radio-file,dsp.img)
$(call add-radio-file,hyp.img)

endif
